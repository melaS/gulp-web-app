# Static Web App
Per creare un web app statica con Sass, jQuery e Modernizer
#### Include:

* Gulp
* BrowserSync
* Browserify
* Sass
* jQuery
* Modernizr

e altri componenti come la compressione delle immagini.

Sentitevi liberi di cambiare, modificare, rimuovere o aggiungere alla vostra build.<br/>
**Questo è solo un punto di partenza.**

***

## Istallazione

Si suppone che si dispone già di **Node.Js** e **Gulp** installati.<br/>
Basta clonare il repository ed eseguire:

	npm install
	
Successivamente si avrà un sistema di compilazione Gulp completamente modulare.

## Uso
***


Ci sono diverse operazioni che è possibile eseguire per gestire il build.

Per eseguire tutti i **tasks**, restare in **watch** sui files e far partire il server:
	
	gulp serve
	
Per eseguire tutti i **tasks** e ottenere una cartella di build con CSS e JS **NON COMPRESSI**:

	gulp
	
Aggiungendo ** --build ** alle **tasks** si ottengono i file **CSS** e **JS** **COMPRESSI**


##### Quindi:
***

Per eseguire tutti i **tasks**, restare in **watch** sui files, far partire il server e ottenere una cartella di build con CSS e JS **COMPRESSI**:
	
	gulp serve --build

Per eseguire tutti i **tasks** e ottenere una cartella di build con CSS e JS **COMPRESSI**:
	
	gulp --build


##### Altre Tasks:
***

Per eseguire la compilazione del sass ed ottenere un file css di nome **style.css** e copiare i **fonts**:
	
	gulp css
	
Per eseguire la concatenazione di tutti i files js in un unico file di nome **main.js** e creare il file **modernizr**:
	
	gulp js

Per ottimizzare tutte le immagini contenute all'interno della cartella "img":
	
	gulp img 
	
Per copiare tutti i file html:

	gulp html
***


	
 
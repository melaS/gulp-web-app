'use strict';

var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    prefix = require('gulp-autoprefixer'),
    gutil = require('gulp-util'),
    handleErrors = require('../utils/handleErrors'),
    browserSync = require('browser-sync'),
    config = require('../utils/config');

var production = gutil.env.build || false;

gulp.task('css:scss', function() {
    return sass(config.css.src + '**/*.scss', {
            sourcemap: true,
            style: production ? 'compressed' : 'nested'
        })
        .pipe(sourcemaps.init())
        .on('error', handleErrors)
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(prefix({
            browsers:['last 3 version', '> 1%', 'Firefox <20', 'Android <=2.3', 'Opera 11-12'],
            cascade: false
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.css.dest))
        .pipe(browserSync.reload({ stream:true }))
});

gulp.task('css:fonts', function() {
   return gulp.src(config.fonts.src + '**/*')
       .on('error', handleErrors)
       .pipe(gulp.dest(config.fonts.dest));
});

gulp.task('css', ['css:scss', 'css:fonts']);
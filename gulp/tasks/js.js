'use strict';

var gulp = require('gulp'),
    modernizr = require('gulp-modernizr'),
    browserify = require('browserify'),
    buffer = require('vinyl-buffer'),
    source = require('vinyl-source-stream'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    handleErrors = require('../utils/handleErrors'),
    config = require('../utils/config');

var production = gutil.env.build || false;


gulp.task('js:browserify', function() {

    var b = browserify({
        debug : true,
        cache: {},
        packageCache: {},
        entries : [config.js.src + 'main.js']
    });

    b.on('update', bundle);
    bundle();

    function bundle() {
        b.bundle()
            .on('error', handleErrors)
            .pipe(source('main.js'))
            .pipe(production ? buffer(): gutil.noop())
            .pipe(production ? uglify({ mangle : false }) : gutil.noop())
            .pipe(gulp.dest(config.js.dest))
            .pipe(reload({stream: true}))

    }

});

gulp.task('js:modernizr', function() {
   return gulp.src(config.js.src + '**/*.js')
       .pipe(modernizr('modernizr-custom.js'))
       .pipe(production ? uglify({ mangle : false  }) : gutil.noop())
       .on('error', handleErrors)
       .pipe(gulp.dest(config.vendor.dest))
});

gulp.task('js', ['js:browserify', 'js:modernizr']);
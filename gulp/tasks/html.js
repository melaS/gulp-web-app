'use strict';

var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    config = require('../utils/config');


gulp.task('html:all', function() {
    return gulp.src(config.html.src + '*.html')
        .pipe(gulp.dest(config.html.dest))
        .pipe(browserSync.reload({ stream:true }))
});

gulp.task('html', ['html:all']);
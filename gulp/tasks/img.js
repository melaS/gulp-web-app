'use strict';

var gulp = require('gulp'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync'),
    config = require('../utils/config');

gulp.task('img:all', function() {
   return gulp.src(config.img.src + '**/*')
       .pipe(imagemin({
           optimizationLevel : 7,
           progressive: true,
           interlaced: true,
           svgoPlugins: [
               {removeViewBox: false}
           ]
       }))
       .pipe(gulp.dest(config.img.dest))
       .pipe(browserSync.reload({ stream:true }));
});

gulp.task('img', ['img:all']);
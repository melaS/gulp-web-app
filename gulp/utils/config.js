module.exports = {
    root : {
        src : "./src/",
        dest : "./build/"
    },
    html : {
        src : "./src/",
        dest : "./build/"
    },
    js : {
        src : "./src/scripts/",
        dest : "./build/scripts/"
    },
    vendor : {
        src : './src/vendor/',
        dest : './build/vendor/'
    },
    css : {
        src : "./src/assets/scss/",
        dest : "./build/assets/css/"
    },
    fonts : {
        src : "./src/assets/fonts/",
        dest : "./build/assets/fonts/"
    },
    img : {
        src : "./src/assets/img/",
        dest : "./build/assets/img/"
    }
};
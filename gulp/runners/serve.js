'use strict';

var gulp = require('gulp'),
    browserSync = require('browser-sync');

var config = require('../utils/config');


function serveInit() {
    return browserSync.init({
        server: {
            baseDir: config.root.dest,
            directory: true
        },
        startPath: 'index.html',
        notify: false
    });
}

gulp.task('serve', ['default'], function() {

    serveInit();

    gulp.watch(config.css.src + '**/*.*', ['css:scss']);
    gulp.watch(config.fonts.src + '**/*.*', ['css:fonts']);
    gulp.watch(config.js.src + '**/*.*', ['js']);
    gulp.watch(config.img.src + '**/*.*', ['img']);
    gulp.watch(config.html.src + '*.html', ['html']);

});
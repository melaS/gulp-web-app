'use strict';

var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    config = require('../utils/config');

var reload = browserSync.reload;

gulp.task('watch', function() {
    gulp.watch(config.css.src + '**/*.*', ['css']).on('chance', reload);
    gulp.watch(config.js.src + '**/*.*', ['js']).on('chance', reload);
});